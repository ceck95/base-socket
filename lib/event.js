const Hoek = require('hoek');

class EventBase {

  constructor(option) {
    Hoek.assert(option.handleClass, 'Event must be class Handle for event this');
    this.handleClass = new option.handleClass();
    this.nameBase = option.nameBase || null;
    this.events = [];
  }

  /**
   * params transmission in function addEvent
   * @param {handle:name,name:string,input:{schema:schema}} cfg 
   */
  addEvent(cfg) {
    let event = cfg || {};
    const handleCurrent = this.handleClass[cfg.handle];
    if (this.nameBase)
      event.name = `${this.nameBase}/${event.name}`;
    if (handleCurrent)
      event.handle = handleCurrent;
    this.events.forEach(e => {
      if (e.name === event.name)
        Hoek.assert(false, `Event name: "${event.name}" exist ,please remove it`);
    });
    this.events.push(event);
  }

  events() {
    return this.events;
  }

}

module.exports = EventBase;
