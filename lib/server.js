const CoreExpress = require('core-express');
const Build = require('../src/build');
const Middleware = require('../src/middleware');
const Emit = require('../src/emit');
const Log = require('../src/log');
const Docs = require('../src/docs');
const fs = require('fs');
const helpers = require('helpers');

class ServerSocket {
  constructor(config) {
    config = config || {};
    this._config = Object.assign(config, {
      onlyApi: true,
      serverSocket: true
    });
  }

  runServer() {
    const ServerExpress = new CoreExpress.Server(this._config, {
      socketHandle: (server, opts) => {
        // const io = require('socket.io').listen(server);
        const io = require('socket.io')(server, {
          handlePreflightRequest: (req, res) => {
            const headers = {
              'Access-Control-Allow-Headers': 'Content-Type, Authorization',
              'Access-Control-Allow-Credentials': true
            };
            if (
              (opts.config.cors && opts.config.cors.open === true) ||
              (Array.isArray(opts.config.cors) &&
                opts.config.cors.includes(req.headers.origin))
            ) {
              if (req.headers.origin) {
                res.setHeader(
                  'Access-Control-Allow-Origin',
                  req.headers.origin
                );
              }
            }
            res.writeHead(200, headers);
            res.end();
          }
        });
        const log = opts.log;
        return Build.buildEvents()
          .then(async events => {
            const pathBase = `${process.cwd()}/sockets/base.js`;
            let handleBase;
            if (fs.existsSync(pathBase)) {
              handleBase = new (require(pathBase))();
            }
            if (handleBase && handleBase.clear) {
              log.debug('Clear data');
              await handleBase.clear(opts);
            }

            io.sockets.on('connection', async socket => {
              socket.reply = Emit.emit;
              socket.log = log;
              socket.replyError = Emit.emitError;
              socket.getStore = opts.getStore;

              if (handleBase && handleBase.connection) {
                log.debug(`Handle connected with socketId: ${socket.id}`);
                await handleBase.connection(io, socket);
              } else {
                log.debug(`Guest connected with socketId: ${socket.id}`);
              }
              events.forEach(event => {
                socket.on(event.name, (...args) => {
                  log.debug('Info socket');
                  socket.logTracking = {
                    socket: {
                      event: {
                        name: event.name,
                        handle: event.handle.name
                      },
                      headers: socket.handshake.headers,
                      body: args
                    }
                  };
                  Log.logTracking(log, socket.logTracking);
                  socket.eventName = event.name;
                  return Middleware.run(socket, event, ...args)
                    .then(errors => {
                      if (errors) return socket.replyError(socket, errors);
                      return event.handle(io, socket, ...args);
                    })
                    .catch(err => {
                      log.error(err);
                      return socket.replyError(socket, err);
                    });
                });
              });

              socket.on('disconnect', async name => {
                const allow = [
                  'client namespace disconnect',
                  'transport close',
                  'ping timeout',
                  'transport error'
                ];
                if (allow.includes(name)) {
                  if (handleBase && handleBase.connection) {
                    log.debug(
                      `Handle disconnected with socketId: ${socket.id}`
                    );
                    await handleBase.disconnect(io, socket);
                  } else {
                    log.debug(`Guest disconnected with socketId: ${socket.id}`);
                  }
                } else {
                  log.error(`Disconnected with name: ${name}`);
                }
              });
            });
          })
          .catch(err => {
            log.error(err);
          });
      },
      buildDocs: () => {
        return Build.buildEvents().then(events => {
          return Docs.json(events);
        });
      }
    });

    ServerExpress.runServer();
  }
}

module.exports = ServerSocket;
