/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-17 09:40:38 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-02-28 15:21:25
 */
const BPromise = require('bluebird');
const fs = BPromise.promisifyAll(require('fs'));
const Hoek = require('hoek');

class Build {
  static buildEvents() {
    return new BPromise((resolve, reject) => {
      const pathName = 'events';
      const fullPath = `${process.cwd()}/${pathName}`;
      let result = [];
      return fs
        .readdirAsync(fullPath)
        .then(files => {
          files.forEach((file, i) => {
            const currentPath = `${fullPath}/${file}`;
            const eventsCurrent = require(currentPath);
            if (result.length > 0)
              result.forEach(e => {
                eventsCurrent.forEach(a => {
                  if (e.name === a.name)
                    Hoek.assert(
                      false,
                      `Event name ${a.name} at path ${currentPath} exist`
                    );
                });
              });
            result = result.concat(eventsCurrent);
            if (files.length === i + 1) {
              return resolve(result);
            }
          });
        })
        .catch(err => {
          return reject(err);
        });
    });
  }

  static authorize(keyAuthorize, socket, event) {
    return new BPromise((resolve, reject) => {
      const rootPath = 'sockets',
        fileAuthorize = 'authorization',
        fullPath = `${process.cwd()}/${rootPath}/${fileAuthorize}.js`;
      return fs.exists(fullPath, exist => {
        if (!exist) return resolve(null);

        if (
          (event.input && event.input.authorized) ||
          (event.input && event.input.unauthorized)
        ) {
          const classAuth = new (require(fullPath))(),
            prom = classAuth[keyAuthorize](socket);
          Hoek.assert(prom.then, 'Function authorize must be a promise');
          return prom
            .then(auth => {
              auth =
                auth !== null &&
                typeof auth === 'object' &&
                Object.keys(auth).length > 0
                  ? auth
                  : null;
              return resolve(auth);
            })
            .catch(err => {
              return reject(err);
            });
        }

        return resolve(null);
      });
    });
  }
}

module.exports = Build;
