const Error = require('core-express').Error;
const BPromise = require('bluebird');
const Build = require('./build');

class Middleware {

  static validateInput(event, ...args) {
    return new BPromise((resolve, reject) => {
      if (!event.input || !event.input.schema)
        return resolve(null);
      let schemas = event.input.schema;
      if (event.input && schemas) {
        let errors = [];
        if (typeof schemas === 'object')
          schemas = [schemas];
        schemas.forEach((e, i) => {
          errors = errors.concat(Error.validate(e, args[i]));
        });
        if (errors.length > 0)
          return resolve(errors);
        return resolve(null);
      }
      return resolve(null);
    });
  }

  static checkAuthorize(socket, event, keyAuthorize) {
    if (event.input) {
      if (event.input.authorized)
        return socket[keyAuthorize] === null ? {
          code: '504',
          source: 'require authorized'
        } : null;
      if (event.input.unauthorized)
        return socket[keyAuthorize] !== null ? {
          code: '500',
          source: 'require authorized'
        } : null;
      return null;
    }
  }

  static run(socket, event, ...args) {
    return new BPromise((resolve, reject) => {
      const keyAuthorize = 'user';
      return Build.authorize(keyAuthorize, socket, event).then(dataAuth => {
        socket[keyAuthorize] = dataAuth;
        const errorAuth = Middleware.checkAuthorize(socket, event, keyAuthorize);

        if (errorAuth)
          return resolve(errorAuth);

        return Middleware.validateInput(event, ...args).then(errors => {
          return resolve(errors);
        });

      }).catch(err => {
        return reject(err);
      });

    });

  }

}

module.exports = Middleware;
