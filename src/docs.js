const helpers = require('helpers');

class Docs {
  static parseObjectJoi(data) {
    let result;
    if (Array.isArray(data)) {
      result = [];
      data.forEach(e => {
        result.push(helpers.Joi.joiToObject(e));
      });
      return result;
    }
    return [helpers.Joi.joiToObject(data)];
  }

  static json(events) {
    return events.map(e => {
      return {
        emit: {
          name: e.name,
          desc: e.desc || 'Default description',
          on: e.emitOn || 'broadcast',
          error: 'emitError'
        },
        input: Docs.parseObjectJoi(e.input ? e.input.schema || [] : [])
      };
    });
  }
}

module.exports = Docs;
