class Log {
  constructor(log) {
    this.log = log;
  }

  extractLogEventSummary(events) {
    this.log.info('Document generate:');
    events.forEach(e => {
      this.log.info('-----------------');
      this.log.info(`Event name: "${e.name}"`);
      if (e.input && e.input.schema) {
        this.log.info('Input:');
        this.log.info(this.parseObjectJoi(e.input.schema));
      }
      this.log.info(`Handle: "${e.handle.name}"`);
      this.log.info('-----------------');
    });
  }

  static logTracking(log, data, level) {
    log[level || 'debug'](data);
  }
}

module.exports = Log;
