/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-16 15:40:58 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-03-28 15:46:41
 */
const Hoek = require('hoek');
const Error = require('core-express').Error;
const Log = require('./log');

class Emit {
  static emit(socket, data, opts) {
    opts = opts || {};
    let result = {
      data: data
    };
    if (opts.message) {
      result.meta = {
        message: opts.message
      };
    }
    return Emit.emitCommon(socket, result, opts);
  }

  static emitCommon(socket, result, opts) {
    const src = opts.emit ? opts.emit : socket,
      emitName = opts.emitName,
      log = socket.log;
    log.debug(`Emit socket "${socket.eventName || 'base'}"`);
    log.debug(result);
    if (Array.isArray(src)) {
      if (src.length === 0) {
        log.debug('Emit empty');
        return true;
      }
      log.debug(`Emit socket array have ${src.length} item`);
      Hoek.assert(false, 'Emit type not working');
      // return src.forEach(e => {
      //   Hoek.assert(e.emit, 'Emit function not exist');
      //   e.emit(emitName, result);
      // });
    } else {
      log.debug('Emit socket default');
      Hoek.assert(src.emit, 'Emit function not exist');
      return src.emit(emitName, result);
    }
  }

  static emitError(socket, errors, opts) {
    opts = opts || {};
    opts.emitName = 'emitError';
    Hoek.assert(errors, 'Emit errors must be data errors');
    const log = socket.log;
    log.error(`Emit socket "${socket.eventName || 'base'}"`);
    log.error('Log tracking:');
    Log.logTracking(log, socket.logTracking, 'error');
    if (!Array.isArray(errors)) errors = Error.translate(errors);
    log.error('Data errors:');
    log.error(errors);
    socket.emit(opts.emitName, errors);
  }
}

module.exports = Emit;
